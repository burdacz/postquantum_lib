List of postquantum algorithms that can be tested: 


KEM/Encryption: BIKE, Crystals-Kyber, Frodo, HQC, LAC, NewHope, NTRUEncrypt, NTS-KEM, ROLLO, ThreeBears


Signatures: Crystals-Dilithium, Falcon, MQDSS, Picnic, qTesla, SPHINCS+




For automatic installation just run ./install

Then start with ./run

For manual installation:

Building algorithms

sudo apt-get install make

sudo apt-get install gcc

sudo apt-get install g++

Memory testing

sudo apt-get install valgrind
 
OpenSSL

sudo apt-get install libssl-dev
 
NTL/gf2x

sudo add-apt-repository universe

sudo apt-get install libntl-dev
 
GMP

sudo apt-get install libgmp3-dev
 
XSLTPROC

sudo apt-get install xsltproc
 
Keccak

git clone https://github.com/gvanas/KeccakCodePackage

cd KeccakCodePackage

make generic64/libkeccak.a (for standard 64-bit architecture)

sudo cp -a bin/generic64/libkeccak.a /usr/local/lib

sudo cp -a bin/generic64/libkeccak.a.headers /usr/local/include
