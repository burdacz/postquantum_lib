+=============================================================================+
                                                                               
                ***********************************************                
                *     POST-QUANTUM CRYPTO STANDARDIZATION     *
                ***********************************************                
                                                                               
 ============================================================================= 
                                                                               
         Proposal: ROLLO (merge of Rank-Ouroboros, LAKE & LOCKER) 
         
         Submitters (by alphabetical order):                         
                   - Carlos AGUILAR MELCHOR                                    
                   - Nicolas ARAGON                                            
                   - Slim BETTAIEB                                             
                   - Loïc BIDOUX                                               
                   - Olivier BLAZY
                   - Jean-Christophe DENEUVILLE
                   - Philippe GABORIT [main contact]
                   - Adrien HAUTEVILLE [backup contact]                           
                   - Olivier RUATTA
                   - Jean-Pierre TILLICH
                   - Gilles ZÉMOR                                              
                                                                               
         Inventors: Same as submitters                                         
                                                                               
         Developers: Same as submitters                                        
                                                                               
         Owners: Same as submitters                                            
                                                                               
+=============================================================================+

This archive is a proposal in response to NIST's call for proposal for standar-
dization of quantum-resistant public-key cryptographic algorithms.

ROLLO is a compilation of three candidates to the NIST's competition for
post-quantum cryptography standardization. They are based on rank metric codes
and they all share the same decoding algorithm for LRPC codes. Rank-Ouroboros
(formely known as Ouroboros-R) and LAKE are IND-CPA KEM running in the category
"post-quantum key exchange". LOCKER is an IND-CCA2 PKE running in the category
"post-quantum public key encryption". Different sets of parameters for these
three cryptosystems are proposed for security strength categories 1, 3 and 5.

===============================================================================

This archive is organized as follows:

|-- KATs
|   |-- Optimized_Implementation
|   |   |-- LAKE-I
|   |   |   |-- PQCkemKAT_40.req
|   |   |   |-- PQCkemKAT_40.rsp
|   |   |   `-- lakeI-verbose.txt
|   |   |-- LAKE-II
|   |   |   |-- PQCkemKAT_40.req
|   |   |   |-- PQCkemKAT_40.rsp
|   |   |   `-- lakeII-verbose.txt
|   |   |-- LAKE-III
|   |   |   |-- PQCkemKAT_40.req
|   |   |   |-- PQCkemKAT_40.rsp
|   |   |   `-- lakeIII-verbose.txt
|   |   |-- LOCKER-I
|   |   |   |-- PQCkemKAT_787.req
|   |   |   |-- PQCkemKAT_787.rsp
|   |   |   `-- lockerI-verbose.txt
|   |   |-- LOCKER-II
|   |   |   |-- PQCkemKAT_1119.req
|   |   |   |-- PQCkemKAT_1119.rsp
|   |   |   `-- lockerII-verbose.txt
|   |   |-- LOCKER-III
|   |   |   |-- PQCkemKAT_1286.req
|   |   |   |-- PQCkemKAT_1286.rsp
|   |   |   `-- lockerIII-verbose.txt
|   |   |-- LOCKER-IV
|   |   |   |-- PQCkemKAT_1050.req
|   |   |   |-- PQCkemKAT_1050.rsp
|   |   |   `-- lockerIV-verbose.txt
|   |   |-- LOCKER-IX
|   |   |   |-- PQCkemKAT_2238.req
|   |   |   |-- PQCkemKAT_2238.rsp
|   |   |   `-- lockerIX-verbose.txt
|   |   |-- LOCKER-V
|   |   |   |-- PQCkemKAT_1379.req
|   |   |   |-- PQCkemKAT_1379.rsp
|   |   |   `-- lockerV-verbose.txt
|   |   |-- LOCKER-VI
|   |   |   |-- PQCkemKAT_1482.req
|   |   |   |-- PQCkemKAT_1482.rsp
|   |   |   `-- lockerVI-verbose.txt
|   |   |-- LOCKER-VII
|   |   |   |-- PQCkemKAT_1679.req
|   |   |   |-- PQCkemKAT_1679.rsp
|   |   |   `-- lockerVII-verbose.txt
|   |   |-- LOCKER-VIII
|   |   |   |-- PQCkemKAT_1977.req
|   |   |   |-- PQCkemKAT_1977.rsp
|   |   |   `-- lockerVIII-verbose.txt
|   |   |-- Rank-Ouroboros-128
|   |   |   |-- rank-ouroboros-128_intermediate_values
|   |   |   |-- rank-ouroboros-128_kat.req
|   |   |   `-- rank-ouroboros-128_kat.rsp
|   |   |-- Rank-Ouroboros-192
|   |   |   |-- rank-ouroboros-192_intermediate_values
|   |   |   |-- rank-ouroboros-192_kat.req
|   |   |   `-- rank-ouroboros-192_kat.rsp
|   |   `-- Rank-Ouroboros-256
|   |       |-- rank-ouroboros-256_intermediate_values
|   |       |-- rank-ouroboros-256_kat.req
|   |       `-- rank-ouroboros-256_kat.rsp
|   `-- Reference_Implementation
|       |-- LAKE-I
|       |   |-- PQCkemKAT_40.req
|       |   |-- PQCkemKAT_40.rsp
|       |   `-- lakeI-verbose.txt
|       |-- LAKE-II
|       |   |-- PQCkemKAT_40.req
|       |   |-- PQCkemKAT_40.rsp
|       |   `-- lakeII-verbose.txt
|       |-- LAKE-III
|       |   |-- PQCkemKAT_40.req
|       |   |-- PQCkemKAT_40.rsp
|       |   `-- lakeIII-verbose.txt
|       |-- LOCKER-I
|       |   |-- PQCkemKAT_787.req
|       |   |-- PQCkemKAT_787.rsp
|       |   `-- lockerI-verbose.txt
|       |-- LOCKER-II
|       |   |-- PQCkemKAT_1119.req
|       |   |-- PQCkemKAT_1119.rsp
|       |   `-- lockerII-verbose.txt
|       |-- LOCKER-III
|       |   |-- PQCkemKAT_1286.req
|       |   |-- PQCkemKAT_1286.rsp
|       |   `-- lockerIII-verbose.txt
|       |-- LOCKER-IV
|       |   |-- PQCkemKAT_1050.req
|       |   |-- PQCkemKAT_1050.rsp
|       |   `-- lockerIV-verbose.txt
|       |-- LOCKER-IX
|       |   |-- PQCkemKAT_2238.req
|       |   |-- PQCkemKAT_2238.rsp
|       |   `-- lockerIX-verbose.txt
|       |-- LOCKER-V
|       |   |-- PQCkemKAT_1379.req
|       |   |-- PQCkemKAT_1379.rsp
|       |   `-- lockerV-verbose.txt
|       |-- LOCKER-VI
|       |   |-- PQCkemKAT_1482.req
|       |   |-- PQCkemKAT_1482.rsp
|       |   `-- lockerVI-verbose.txt
|       |-- LOCKER-VII
|       |   |-- PQCkemKAT_1679.req
|       |   |-- PQCkemKAT_1679.rsp
|       |   `-- lockerVII-verbose.txt
|       |-- LOCKER-VIII
|       |   |-- PQCkemKAT_1977.req
|       |   |-- PQCkemKAT_1977.rsp
|       |   `-- lockerVIII-verbose.txt
|       |-- Rank-Ouroboros-128
|       |   |-- rank-ouroboros-128_intermediate_values
|       |   |-- rank-ouroboros-128_kat.req
|       |   `-- rank-ouroboros-128_kat.rsp
|       |-- Rank-Ouroboros-192
|       |   |-- rank-ouroboros-192_intermediate_values
|       |   |-- rank-ouroboros-192_kat.req
|       |   `-- rank-ouroboros-192_kat.rsp
|       `-- Rank-Ouroboros-256
|           |-- rank-ouroboros-256_intermediate_values
|           |-- rank-ouroboros-256_kat.req
|           `-- rank-ouroboros-256_kat.rsp
|-- Optimized_Implementation
|   |-- LAKE-I
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- lake_types.h
|   |       |-- main_kat.c
|   |       |-- main_lake.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LAKE-II
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- lake_types.h
|   |       |-- main_kat.c
|   |       |-- main_lake.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LAKE-III
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- lake_types.h
|   |       |-- main_kat.c
|   |       |-- main_lake.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LAKE-README.md
|   |-- LOCKER-I
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-II
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-III
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-IV
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-IX
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-README.md
|   |-- LOCKER-V
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-VI
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-VII
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-VIII
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- Rank-Ouroboros-128
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- main_kat.c
|   |       |-- main_rank_ouroboros.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       |-- parsing.h
|   |       |-- qcrs_recoverer.cpp
|   |       `-- qcrs_recoverer.h
|   |-- Rank-Ouroboros-192
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- main_kat.c
|   |       |-- main_rank_ouroboros.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       |-- parsing.h
|   |       |-- qcrs_recoverer.cpp
|   |       `-- qcrs_recoverer.h
|   |-- Rank-Ouroboros-256
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- main_kat.c
|   |       |-- main_rank_ouroboros.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       |-- parsing.h
|   |       |-- qcrs_recoverer.cpp
|   |       `-- qcrs_recoverer.h
|   `-- Rank-Ouroboros-README
|-- README
|-- Reference_Implementation
|   |-- LAKE-I
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- lake_types.h
|   |       |-- main_kat.c
|   |       |-- main_lake.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LAKE-II
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- lake_types.h
|   |       |-- main_kat.c
|   |       |-- main_lake.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LAKE-III
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- lake_types.h
|   |       |-- main_kat.c
|   |       |-- main_lake.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LAKE-README.md
|   |-- LOCKER-I
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-II
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-III
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-IV
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-IX
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-README.md
|   |-- LOCKER-V
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-VI
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-VII
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- LOCKER-VIII
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- decoder.cpp
|   |       |-- decoder.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- locker_types.h
|   |       |-- main_kat.c
|   |       |-- main_locker.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       `-- parsing.h
|   |-- Rank-Ouroboros-128
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- main_kat.c
|   |       |-- main_rank_ouroboros.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       |-- parsing.h
|   |       |-- qcrs_recoverer.cpp
|   |       `-- qcrs_recoverer.h
|   |-- Rank-Ouroboros-192
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- main_kat.c
|   |       |-- main_rank_ouroboros.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       |-- parsing.h
|   |       |-- qcrs_recoverer.cpp
|   |       `-- qcrs_recoverer.h
|   |-- Rank-Ouroboros-256
|   |   |-- Makefile
|   |   |-- doc
|   |   |   |-- main_page.txt
|   |   |   `-- refs.bib
|   |   |-- doxygen.conf
|   |   |-- lib
|   |   |   |-- hash
|   |   |   |   |-- hash.c
|   |   |   |   `-- hash.h
|   |   |   `-- rng
|   |   |       |-- rng.c
|   |   |       `-- rng.h
|   |   `-- src
|   |       |-- api.h
|   |       |-- ffi
|   |       |   |-- ffi.h
|   |       |   |-- ffi_elt.cpp
|   |       |   |-- ffi_elt.h
|   |       |   |-- ffi_field.cpp
|   |       |   |-- ffi_field.h
|   |       |   |-- ffi_vec.cpp
|   |       |   `-- ffi_vec.h
|   |       |-- kem.cpp
|   |       |-- main_kat.c
|   |       |-- main_rank_ouroboros.cpp
|   |       |-- parameters.h
|   |       |-- parsing.cpp
|   |       |-- parsing.h
|   |       |-- qcrs_recoverer.cpp
|   |       `-- qcrs_recoverer.h
|   `-- Rank-Ouroboros-README
|-- Supporting_Documentation
|   |-- ROLLO_Specification.pdf
`-- tmp

===============================================================================

The authors did their best to make this archive complete and proper.
    
