

---------------------------------------------------- 
Rank-Ouroboros: a Key Encapsulation Mechanism scheme 
----------------------------------------------------


1. SUBMISSION OVERVIEW 
----------------------

Rank-Ouroboros is a PR-CPA secure Key Encapsulation Mechanism scheme. Both
reference implementation and optimized implementation provided for this
submission are the same. Three parameters sets denoted respectively
Rank-Ouroboros-128, Rank-Ouroboros-192 and Rank-Ouroboros-256 are provided as explained
in the supporting documentation. Each parameter set folder is organized as
follows:

- bin/: Files generated during compilation
- doc/: Technical documentation of the scheme
- lib/: Third party libraries used 
- src/: Source code of the scheme  
- doxygen.conf: Documentation configuration file
- configure: Configuration file
- Makefile: Makefile


2. INSTALLATION INSTRUCTIONS 
----------------------------

2.1 Requirements

The following softwares and librairies are required: make, g++, openssl, ntl and
gmp.

2.2 Compilation Step

Let X denotes 128, 192 or 256 depending on the parameter set considered.
Rank-Ouroboros can be compiled in three differents ways:
- Execute make ouroboros-r-X to compile a working example of the scheme. Run
  bin/ouroboros-r-X to execute the scheme.
- Execute make ouroboros-r-X-kat to compile the NIST KAT generator. Run
  bin/ouroboros-r-X-kat to generate KAT files.
- Execute make ouroboros-r-X-verbose to compile a working example of the scheme in
  verbose mode. Run bin/ouroboros-r-X-verbose to generate intermediate values.

During compilation, the following files are created inside the bin/build folder:
- hash.o: A wrapper around openssl SHA512 implementation
- rng.o: NIST rng
- ffi_field.o: Functions to manipulate finite fields.
- ffi_elt.o: Functions to manipulate finite fields elements.
- ffi_vec.o: Functions to manipulate vectors over finite fields.
- parsing.o: Functions to parse public key, secret key and ciphertext of the
  scheme.
- qcrs_recoverer.o: Functions to solve the quasi-cyclic rank support recovery
  problem (either in normal mode or verbose mode).
- kem.o: The Rank-Ouroboros scheme (either in normal mode or verbose mode).


3. DOCUMENTATION GENERATION 
---------------------------

3.1 Requirements

The following softwares are required: doxygen and bibtex.

3.2 Generation Step

- Run doxygen doxygen.conf to generate the code documentation
- Browse doc/html/index.html to read the documentation


4. ADDITIONAL INFORMATION 
-------------------------

4.1 Implementation overview

The Rank-Ouroboros scheme is defined in the api.h and parameters.h files and
implemented in kem.cpp. During the decapsulation step, it uses a quasi-cyclic
rank support recoverer (see qcrs_recoverer.h and qcrs_recoverer.cpp). The files
ffi.h, ffi_field.h, ffi_elt.h, ffi_vec.h, ffi_field.cpp, ffi_elt.cpp and
ffi_vec.cpp provide the functions performing the various operations over finite
fields required by the scheme. As public key, secret key and ciphertext can
manipulated either with theirs mathematical representations or as bit strings,
the files parsing.h and parsing.cpp provide functions to switch between these
two representations. Finally, the files hash.h, rng.h, hash.c and rng.c (inside
the lib/ folder) contain respectively a wrapper around OpenSSL SHA512
implementation and the NIST random functions.  

4.2 Finite field interface

Rank-Ouroboros is a rank-based scheme and as such heavily relies on finite field
arithmetic. We have provided an interface for finite fields (through files
ffi.h, ffi_field.h, ffi_elt.h and ffi_vec.h) describing the various operations
required by the scheme. In this submission, the finite field interface (ffi) is
implemented using the NTL library but one should note that Rank-Ouroboros can work
with any implementation of our finite field interface as long as its API is
respected. In the context of our ffi interface, a finite field always describes
an extension of a binary field namely a finite field of the form GF(2^m).

The ffi interface works as follows:
- ffi.h: Constants that defines the considered finite field ;
- ffi_field.h: Functions to initialize finite fields ;
- ffi_elt.h: Functions to manipulate elements of GF(2^m) ;
- ffi_vec.h: Functions to manipulate either vectors over GF(2^m) or vector
  spaces spanned by a vector over GF(2^m).

In our reference implementation, the considered fields are GF(2^89), GF(2^101)
and GF(2^127) for Rank-Ouroboros-128, Rank-Ouroboros-192 and Rank-Ouroboros-256. In
addition, the ffi_elt and ffi_vec types are respectively implemented as NTL GF2E
and GF2EX types.

4.3 Public key, secret key, ciphertext and shared secret

The public key, secret key and ciphertext are respectively composed of the
vectors (h, s), (x, y) and (sr, se). The shared secret is the output of the hash
of r1, r2 and er using SHA512. In order to shorten the keys, the public key is
stored as (seed1, s) and the secret key is stored as (seed2). To this end, the
seed expander provided by the NIST was used along with 40 bytes long seeds. The
representation we choose to store s, sr and se as bit strings may contain
unecessary zeros that can be removed if necessary.

