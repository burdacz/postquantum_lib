/**
 * \file ffi_field.h
 * \brief Interface for finite fields
 *
 * In the context of the Ouroboros scheme, a finite field always describes an extension of a binary field namely a finite field of the form \f$ GF(2^m) \f$.
 *
 * Finite fields used for Ouroboros-128, Ouroboros-192 and Ouroboros-256 are respectively \f$ GF(2^{89}) \f$, \f$ GF(2^{101}) \f$ and \f$ GF(2^{127}) \f$.
 *
 */

#ifndef FFI_FIELD_H
#define FFI_FIELD_H


void ffi_field_init();

#endif

