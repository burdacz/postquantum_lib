/**
 * \file ffi_field.cpp
 * \brief Interface for finite fields
 *
 * In the context of the Ouroboros scheme, a finite field always describes an extension of a binary field namely a finite field of the form \f$ GF(2^m) \f$.
 *
 * Finite fields used for Ouroboros-128, Ouroboros-192 and Ouroboros-256 are respectively \f$ GF(2^{89}) \f$, \f$ GF(2^{101}) \f$ and \f$ GF(2^{127}) \f$.
 *
 */

#include <NTL/GF2E.h>
#include <NTL/GF2X.h>
#include <NTL/GF2XFactoring.h>
#include "ffi.h"
#include "ffi_field.h"

using namespace NTL;


/** 
 * \fn void ffi_field_init()
 * \brief This function initializes a finite field
 */
void ffi_field_init() {
  GF2X P = BuildSparseIrred_GF2X(FIELD_M);
  GF2E::init(P);

  GF2X::HexOutput = 1;
}

