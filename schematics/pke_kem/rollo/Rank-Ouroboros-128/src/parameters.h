/** 
 * \file parameters.h
 * \brief Parameters of the Ouroboros-R scheme
 */

#ifndef OUROBOROS_PARAMETER_H
#define OUROBOROS_PARAMETER_H


#define PARAM_Q 2 /**< Parameter q of the scheme (finite field GF(q^m)) */
#define PARAM_M 89 /**< Parameter m of the scheme (finite field GF(q^m)) */
#define PARAM_N 53 /**< Parameter n of the scheme (code length) */
#define PARAM_W 5 /**< Parameter omega of the scheme (weight of vectors) */
#define PARAM_R 6 /**< Parameter omega_r of the scheme (weight of vectors) */
#define PARAM_DFR 36 /**< Decryption Failure Rate (2^-36) */
#define PARAM_SECURITY 128 /**< Expected security level */

#define SECRET_KEY_BYTES CRYPTO_SECRETKEYBYTES /**< Secret key size */
#define PUBLIC_KEY_BYTES CRYPTO_PUBLICKEYBYTES /**< Public key size */
#define SHARED_SECRET_BYTES CRYPTO_BYTES /**< Shared secret size */
#define CIPHERTEXT_BYTES CRYPTO_CIPHERTEXTBYTES /**< Ciphertext size */

#define FFI_VEC_R_BYTES PARAM_R * (PARAM_M / 8 + 1) /**< Number of bytes required to store an NTL vector of size r */
#define FFI_VEC_N_BYTES PARAM_N * (PARAM_M / 8 + 1) /**< Number of bytes required to store an NTL vector of size n */

#define SHA512_BYTES 64 /**< Size of SHA512 output */

#define SEEDEXPANDER_SEED_BYTES 40 /**< Seed size of the NIST seed expander */
#define SEEDEXPANDER_MAX_LENGTH 4294967295 /**< Max length of the NIST seed expander */

#endif

