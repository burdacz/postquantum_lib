/** 
 * \file qcrs_recoverer.h
 * \brief Functions for quasi-cyclic rank support recovery problem
 */

#ifndef QCRS_RECOVERER_H
#define QCRS_RECOVERER_H

#include "ffi_vec.h"

void qcrs_recoverer(ffi_vec& E, unsigned int& E_dim, unsigned int E_expected_dim, const ffi_vec& F, unsigned int F_dim, const ffi_vec& ec, unsigned int ec_size);

#endif

