#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SUB_ALG_COUNT 18
#define ALG_COUNT 16
#define SIGNATURE_INDEX 10

struct algorithmSub
{
   char* name;
   char* path;
   int nistLvl;
};

struct algorithm
{
   char* name;
   char* docName;
   char* path;
   int subCount;

   struct algorithmSub algorithmSubs[SUB_ALG_COUNT];
};

struct algorithm algorithms[ALG_COUNT];
struct algorithmSub subs[SUB_ALG_COUNT];
int runs = 100;

void initialData()
{
    // PKE_KEM ====================================================================================

    fillAlgorithmSub(0, "BIKE1_1", "bike_1_1", 1);
    fillAlgorithmSub(1, "BIKE1_3", "bike_1_3", 3);
    fillAlgorithmSub(2, "BIKE1_5", "bike_1_5", 5);
    fillAlgorithm(0, "BIKE", "bike", "bike", subs, 3);

    fillAlgorithmSub(0, "Kyber512", "kyber512", 2);
    fillAlgorithmSub(1, "Kyber768", "kyber768", 3);
    fillAlgorithmSub(2, "Kyber1024", "kyber1024", 4);
    fillAlgorithm(1, "Crystals-Kyber", "crystals-kyber", "crystals-kyber", subs, 3);

    fillAlgorithmSub(0, "FrodoKEM640", "frodoKEM640", 1);
    fillAlgorithmSub(1, "FrodoKEM976", "frodoKEM976", 1);
    fillAlgorithm(2, "Frodo", "frodo", "frodo", subs, 2);

    fillAlgorithmSub(0, "HQC-128-1", "hqc-128-1", 1);
    fillAlgorithmSub(1, "HQC-192-1", "hqc-192-1", 3);
    fillAlgorithmSub(2, "HQC-192-2", "hqc-192-2", 3);
    fillAlgorithmSub(3, "HQC-256-1", "hqc-256-1", 5);
    fillAlgorithmSub(4, "HQC-256-2", "hqc-256-2", 5);
    fillAlgorithmSub(5, "HQC-256-3", "hqc-256-3", 5);
    fillAlgorithm(3, "HQC", "hqc", "hqc", subs, 6);

    fillAlgorithmSub(0, "LAC128", "lac128", 1);
    fillAlgorithmSub(1, "LAC192", "lac192", 3);
    fillAlgorithmSub(2, "LAC256", "lac256", 5);
    fillAlgorithm(4, "LAC", "lac", "lac", subs, 3);

    fillAlgorithmSub(0, "NewHope512CCA", "newhope512cca", 3);
    fillAlgorithmSub(1, "NewHope512CPA", "newhope512cpa", 3);
    fillAlgorithmSub(2, "NewHope1024CCA", "newhope1024cca", 5);
    fillAlgorithmSub(3, "NewHope1024CPA", "newhope1024cpa", 5);
    fillAlgorithm(5, "NewHope", "newhope", "newhope", subs, 4);

    fillAlgorithmSub(0, "NTRU-kem-443", "ntru-kem-443", 1);
    fillAlgorithmSub(1, "NTRU-kem-743", "ntru-kem-743", 5);
    fillAlgorithmSub(2, "NTRU-kem-1024", "ntru-kem-1024", 5);
    fillAlgorithmSub(3, "NTRU-pke-443", "ntru-pke-443", 1);
    fillAlgorithmSub(4, "NTRU-pke-743", "ntru-kem-743", 5);
    fillAlgorithmSub(5, "NTRU-pke-1024", "ntru-pke-1024", 5);
    fillAlgorithm(6, "NTRUEncrypt", "ntruencrypt", "ntruencrypt", subs, 6);

    fillAlgorithmSub(0, "NTS-KEM-12-64", "nts_kem_12_64", 1);
    fillAlgorithmSub(1, "NTS-KEM-13-80", "nts_kem_13_80", 3);
    fillAlgorithmSub(2, "NTS-KEM-13-136", "nts_kem_13_136", 5);
    fillAlgorithm(7, "NTS-KEM", "nts-kem", "nts-kem", subs, 3);

    fillAlgorithmSub(0, "LAKE-I", "LAKE-I", 1);
    fillAlgorithmSub(1, "LAKE-II", "LAKE-II", 3);
    fillAlgorithmSub(2, "LAKE-III", "LAKE-III", 5);
    fillAlgorithmSub(3, "LOCKER-I", "LOCKER-I", 1);
    fillAlgorithmSub(4, "LOCKER-II", "LOCKER-II", 3);
    fillAlgorithmSub(5, "LOCKER-III", "LOCKER-III", 5);
    fillAlgorithmSub(6, "LOCKER-IV", "LOCKER-IV", 1);
    fillAlgorithmSub(7, "LOCKER-V", "LOCKER-V", 3);
    fillAlgorithmSub(8, "LOCKER-VI", "LOCKER-VI", 5);
    fillAlgorithmSub(9, "LOCKER-VII", "LOCKER-VII", 1);
    fillAlgorithmSub(10, "LOCKER-VIII", "LOCKER-VIII", 3);
    fillAlgorithmSub(11, "LOCKER-IX", "LOCKER-IX", 5);
    fillAlgorithmSub(12, "Rank-Ouroboros-128", "Rank-Ouroboros-128", 1);
    fillAlgorithmSub(13, "Rank-Ouroboros-192", "Rank-Ouroboros-192", 3);
    fillAlgorithmSub(14, "Rank-Ouroboros-256", "Rank-Ouroboros-256", 5);
    fillAlgorithm(8, "ROLLO", "rollo", "rollo", subs, 15);

    fillAlgorithmSub(0, "BabyBear", "BabyBear", 2);
    fillAlgorithmSub(1, "BabyBearEphem", "BabyBearEphem", 2);
    fillAlgorithmSub(2, "MamaBear", "MamaBear", 4);
    fillAlgorithmSub(3, "MamaBearEphem", "MamaBearEphem", 4);
    fillAlgorithmSub(4, "PapaBear", "PapaBear", 5);
    fillAlgorithmSub(5, "PapaBearEphem", "PapaBearEphem", 5);
    fillAlgorithm(9, "ThreeBears", "threebears", "threebears", subs, 6);

    // SIGNATURES ====================================================================================

     fillAlgorithmSub(0, "Dilithium-weak", "Dilithium_weak", 1);
     fillAlgorithmSub(1, "Dilithium-medium", "Dilithium_medium", 1);
     fillAlgorithmSub(2, "Dilithium-recommended", "Dilithium_recommended", 2);
     fillAlgorithmSub(3, "Dilithium-veryhigh", "Dilithium_very_high", 3);
     fillAlgorithm(10, "Crystals-Dilithium", "crystals-dilithium", "crystals-dilithium", subs, 4);

     fillAlgorithmSub(0, "Falcon-512", "falcon512", 1);
     fillAlgorithmSub(1, "Falcon-768", "falcon768", 3);
     fillAlgorithmSub(2, "Falcon-1024", "falcon1024", 5);
     fillAlgorithm(11, "Falcon", "falcon", "falcon", subs, 3);

     fillAlgorithmSub(0, "MQDSS-48", "mqdss-48", 2);
     fillAlgorithmSub(1, "MQDSS-64", "mqdss-64", 4);
     fillAlgorithm(12, "MQDSS", "mqdss", "mqdss", subs, 2);

     fillAlgorithmSub(0, "Picnic-l1fs", "picnicl1fs", 1);
     fillAlgorithmSub(1, "Picnic-l1ur", "picnicl1ur", 1);
     fillAlgorithmSub(2, "Picnic-l3fs", "picnicl3fs", 3);
     fillAlgorithmSub(3, "Picnic-l3ur", "picnicl3ur", 3);
     fillAlgorithmSub(4, "Picnic-l5fs", "picnicl5fs", 5);
     fillAlgorithmSub(5, "Picnic-l5ur", "picnicl5ur", 5);
     fillAlgorithm(13, "Picnic", "picnic", "picnic", subs, 6);

     fillAlgorithmSub(0, "qTesla-128", "qTesla_128", 1);
     fillAlgorithmSub(1, "qTesla-192", "qTesla_192", 3);
     fillAlgorithmSub(2, "qTesla-256", "qTesla_256", 5);
     fillAlgorithm(14, "qTesla", "qtesla", "qtesla", subs, 3);

     fillAlgorithmSub(0, "SPHINCS-Haraka-128f", "sphincs-haraka-128f", 1);
     fillAlgorithmSub(1, "SPHINCS-Haraka-128s", "sphincs-haraka-128s", 1);
     fillAlgorithmSub(2, "SPHINCS-Haraka-192f", "sphincs-haraka-192f", 3);
     fillAlgorithmSub(3, "SPHINCS-Haraka-192s", "sphincs-haraka-192s", 3);
     fillAlgorithmSub(4, "SPHINCS-Haraka-256f", "sphincs-haraka-256f", 5);
     fillAlgorithmSub(5, "SPHINCS-Haraka-256s", "sphincs-haraka-256s", 5);
     fillAlgorithmSub(6, "SPHINCS-Sha256-128f", "sphincs-sha256-128f", 1);
     fillAlgorithmSub(7, "SPHINCS-Sha256-128s", "sphincs-sha256-128s", 1);
     fillAlgorithmSub(8, "SPHINCS-Sha256-192f", "sphincs-sha256-192f", 3);
     fillAlgorithmSub(9, "SPHINCS-Sha256-192s", "sphincs-sha256-192s", 3);
     fillAlgorithmSub(10, "SPHINCS-Sha256-256f", "sphincs-sha256-256f", 5);
     fillAlgorithmSub(11, "SPHINCS-Sha256-256s", "sphincs-sha256-256s", 5);
     fillAlgorithmSub(12, "SPHINCS-Shake256-128f", "sphincs-shake256-128f", 1);
     fillAlgorithmSub(13, "SPHINCS-Shake256-128s", "sphincs-shake256-128s", 1);
     fillAlgorithmSub(14, "SPHINCS-Shake256-192f", "sphincs-shake256-192f", 3);
     fillAlgorithmSub(15, "SPHINCS-Shake256-192s", "sphincs-shake256-192s", 3);
     fillAlgorithmSub(16, "SPHINCS-Shake256-256f", "sphincs-shake256-256f", 5);
     fillAlgorithmSub(17, "SPHINCS-Shake256-256s", "sphincs-shake256-256s", 5);
     fillAlgorithm(15, "SPHINCS+", "sphincsplus", "sphincsplus", subs, 18);
}

void fillAlgorithm(int index, char* name, char* docName, char* path, struct algorithmSub subs[SUB_ALG_COUNT], int subCount)
{
    algorithms[index].name = name;
    algorithms[index].docName = docName;
    algorithms[index].path = path;
    algorithms[index].subCount = subCount;

    for(int i = 0; i < subCount; i++)
    {
        algorithms[index].algorithmSubs[i] = subs[i];
    }

     memset(subs, 0, sizeof subs);
}

void fillAlgorithmSub(int index, char* name, char* path, int nistLvl)
{
    subs[index].name = name;
    subs[index].path = path;
    subs[index].nistLvl = nistLvl;
}

void printMenu()
{
    system("clear");
    printf("Welcome to PQ benchmark library!\n");
    printf("1. List of available algorithms\n");
    printf("2. Speed tests\n");
    printf("3. Memory tests\n");
    printf("4. Documentation\n");
    printf("Please select: \n");
    int input = -1;
    int validInput = scanf("%d", &input);

    if(validInput == 1)
    {
        switch(input)
        {
            case 1:
                printLists();
            break;
            case 2:
		printf("Select number of algorithm runs: ");
		int input1 = -1;
    		int validInput1 = scanf("%d", &input1);
		if(validInput1 == 1)
		{
			runs = input1;

			FILE *f = fopen("runs.txt", "w");
			if (f == NULL)
			{
			    printf("Error opening file!\n");
			    exit(1);
			}
			fprintf(f, "%d", runs);
			fclose(f);

			printf("1. Test all algorithms\n");
    			printf("2. Select algorithm to test\n");
			int input2 = -1;
	    		int validInput2 = scanf("%d", &input2);
			if(validInput2 == 1)
			{
			    switch(input2)
			    {
				case 1:
					system("clear");
					printf("Building/checking files, please wait!\n\n");
		        		buildAlgs();
				break;
				case 2:
					printAndTestAlgorithms();
				break;
				default:
		        		printf("Wrong input!");
		    		break;
			    }
			}
		}
            break;
	    case 3:
			runs = 1;

			FILE *f = fopen("runs.txt", "w");
			if (f == NULL)
			{
			    printf("Error opening file!\n");
			    exit(1);
			}
			fclose(f);
		system("clear");
		for(int i = 0; i < ALG_COUNT; i++)
    		{
    	    		printf("%d. %s \n", (i + 1), algorithms[i].name);
    		}
		
		printf("Select an algorithm for memory test:");
		int index = 0;
		scanf("%d", &index);
		system("clear");
		for(int j = 0; j < algorithms[index - 1].subCount; j++)
		{
			printf("%d. %s \n", (j + 1), algorithms[index - 1].algorithmSubs[j].name);
		}
		printf("Select an algorithm for memory test:");
		int index2 = 0;
		scanf("%d", &index2);
		memoryTest((index - 1), (index2 - 1));
    		break;
            case 4:
                printLists();
                int n = 0;
                int v = scanf("%d", &n);
                if(v == 1)
                {
                    char* path;
                    asprintf(&path, "%s%s", "schematics/documentation/", algorithms[(n-1)].docName);
                    openDoc(path);
                }
            break;
            default:
                printf("Wrong input!");
            break;
        }
    }
}

void memoryTest(int algIndex, int subAlgIndex)
{
	system("clear");
	char* command;
	char* path;
    	int exists = 0;

	if(algIndex >= SIGNATURE_INDEX)
    	{
		asprintf(&path, "%s%s%s%s%s", "schematics/signature/", algorithms[algIndex].path, "/", algorithms[algIndex].algorithmSubs[subAlgIndex].path, "/build");
		exists = cfileexists(path);
		if(!exists)
		{
			asprintf(&command, "%s%s%s%s%s", "cd schematics/signature/", algorithms[algIndex].path, "/", algorithms[algIndex].algorithmSubs[subAlgIndex].path, " && make --silent");
			system(command);
		}
		asprintf(&command, "%s%s%s%s%s", "cd schematics/signature/", algorithms[algIndex].path, "/", algorithms[algIndex].algorithmSubs[subAlgIndex].path, " && valgrind ./build >/dev/null");
    	}
    	else
    	{
		asprintf(&path, "%s%s%s%s%s", "schematics/pke_kem/", algorithms[algIndex].path, "/", algorithms[algIndex].algorithmSubs[subAlgIndex].path, "/build");
		exists = cfileexists(path);
		if(!exists)
		{
			asprintf(&command, "%s%s%s%s%s", "cd schematics/pke_kem/", algorithms[algIndex].path, "/", algorithms[algIndex].algorithmSubs[subAlgIndex].path, " && make --silent");
			system(command);
		}
		asprintf(&command, "%s%s%s%s%s", "cd schematics/pke_kem/", algorithms[algIndex].path, "/", algorithms[algIndex].algorithmSubs[subAlgIndex].path, " && valgrind ./build >/dev/null");
    	}

    	system(command);
}

void openDoc(char* path)
{
    char* command;
    asprintf(&command, "%s%s%s", "xdg-open ", path, ".pdf");
    system(command);
}

void buildAlg(int index)
{
	system("clear");
	printf("Building/checking files, please wait!\n\n");

    	char* command;
	char* path;
    	int exists = 0;
	for(int j = 0; j < algorithms[index].subCount; j++)
	{
	    if(index >= SIGNATURE_INDEX)
	    {
		asprintf(&path, "%s%s%s%s%s", "schematics/signature/", algorithms[index].path, "/", algorithms[index].algorithmSubs[j].path, "/build");
		exists = cfileexists(path);
		if(exists)
		{
			continue;
		}
		asprintf(&command, "%s%s%s%s%s", "cd schematics/signature/", algorithms[index].path, "/", algorithms[index].algorithmSubs[j].path, " && make --silent");
	    }
	    else
	    {
		asprintf(&path, "%s%s%s%s%s", "schematics/pke_kem/", algorithms[index].path, "/", algorithms[index].algorithmSubs[j].path, "/build");
		exists = cfileexists(path);
		if(exists)
		{
			continue;
		}
		asprintf(&command, "%s%s%s%s%s", "cd schematics/pke_kem/", algorithms[index].path, "/", algorithms[index].algorithmSubs[j].path, " && make --silent");
	    }
	    exists = 0;
	    system(command);
	}

	system("clear");
    	printf("Done, now running tests (time is average of %d runs)\n\n", (runs + 1));

    	runAlg(index);
}

void buildAlgs()
{
    char* command;
    char* path;
    int exists = 0;
    for(int i = 0; i < ALG_COUNT; i++)
    {
	asprintf(&command, "%s%d%s%d", "echo ", (i + 1) , "/", ALG_COUNT);
	system(command);
        for(int j = 0; j < algorithms[i].subCount; j++)
        {
            if(i >= SIGNATURE_INDEX)
            {
		asprintf(&path, "%s%s%s%s%s", "schematics/signature/", algorithms[i].path, "/", algorithms[i].algorithmSubs[j].path, "/build");
		exists = cfileexists(path);
		if(exists)
		{
			continue;
		}
                asprintf(&command, "%s%s%s%s%s", "cd schematics/signature/", algorithms[i].path, "/", algorithms[i].algorithmSubs[j].path, " && make --silent");
            }
            else
            {
		asprintf(&path, "%s%s%s%s%s", "schematics/pke_kem/", algorithms[i].path, "/", algorithms[i].algorithmSubs[j].path, "/build");
		exists = cfileexists(path);
		if(exists)
		{
			continue;
		}
                asprintf(&command, "%s%s%s%s%s", "cd schematics/pke_kem/", algorithms[i].path, "/", algorithms[i].algorithmSubs[j].path, " && make --silent");
            }
	    exists = 0;
            system(command);
        }
    }

    system("clear");

    printf("Done, now running tests (time is average of %d runs)\n\n", runs);

    runAlgs();
}

void runAlg(int index)
{
	char* command;
	for(int j = 0; j < algorithms[index].subCount; j++)
        {
	    if(j == 0)
	    {
		asprintf(&command, "%s%s", "echo ", algorithms[index].name);
	        system(command);
	    }
	    if(index >= SIGNATURE_INDEX)
	    {
	        asprintf(&command, "%s%s%s%s%s%s%s", "echo '\t'", algorithms[index].algorithmSubs[j].name, " && cd schematics/signature/", algorithms[index].path, "/", algorithms[index].algorithmSubs[j].path, " && ./build");
	    }
	    else
	    {
	        asprintf(&command, "%s%s%s%s%s%s%s", "echo '\t'", algorithms[index].algorithmSubs[j].name, " && cd schematics/pke_kem/", algorithms[index].path, "/", algorithms[index].algorithmSubs[j].path, " && ./build");
	    }

	    system(command);
	    printf("\n");
        }
}

void runAlgs()
{
    char* command;

    for(int i = 0; i < ALG_COUNT; i++)
    {
        for(int j = 0; j < algorithms[i].subCount; j++)
        {
	    if(j == 0)
	    {
		asprintf(&command, "%s%s", "echo ", algorithms[i].name);
	        system(command);
	    }
	    if(i >= SIGNATURE_INDEX)
	    {
	        asprintf(&command, "%s%s%s%s%s%s%s", "echo '\t'", algorithms[i].algorithmSubs[j].name, " && cd schematics/signature/", algorithms[i].path, "/", algorithms[i].algorithmSubs[j].path, " && ./build");
	    }
	    else
	    {
	        asprintf(&command, "%s%s%s%s%s%s%s", "echo '\t'", algorithms[i].algorithmSubs[j].name, " && cd schematics/pke_kem/", algorithms[i].path, "/", algorithms[i].algorithmSubs[j].path, " && ./build");
	    }

	    system(command);
	    printf("\n");
        }
        printf("\n");
    }
}

void printAndTestAlgorithms()
{
    system("clear");

    for(int i = 0; i < ALG_COUNT; i++)
    {
        printf("%d. %s \n", (i + 1), algorithms[i].name);
    }

    printf("\nSelect an algorithm to test: ");

	int input = -1;
	int validInput = scanf("%d", &input);
	input = input - 1;
	if(validInput == 1)
	{
		buildAlg(input);
	}
}

void printLists()
{
    system("clear");

    for(int i = 0; i < ALG_COUNT; i++)
    {
        printf("%d. %s \n", (i + 1), algorithms[i].name);
        for(int j = 0; j < algorithms[i].subCount; j++)
        {
            printf("\t%s NIST SEC Level:%d \n", algorithms[i].algorithmSubs[j].name, algorithms[i].algorithmSubs[j].nistLvl);
        }
    }
}

int cfileexists(const char * filename){
    if( access( filename, F_OK ) != -1 ) { 
        return 1;
    }
    return 0;
}
